package models

import (
	cm "gitlab.com/dmva/geotask/module/courier/models"
	om "gitlab.com/dmva/geotask/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
